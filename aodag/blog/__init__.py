#
from pyramid.config import Configurator


def includeme(config):
    config.add_route('top', '/')
    config.add_route('new_entry', '/new_entry')
    config.add_route('entries', '/entries')
    config.add_route('entry', '/entry/{slug}')


def main(global_conf, **settings):
    settings.update({'mako.directories': ['aodag.blog:templates']})
    config = Configurator(settings=settings)
    config.include("pyramid_mako")
    config.include(".")
    config.scan(".views")
    return config.make_wsgi_app()
