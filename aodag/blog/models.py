from datetime import datetime
from sqlalchemy import (
    BLOB,
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Unicode,
    UnicodeText,
)
from sqlalchemy.orm import (
    sessionmaker,
    scoped_session,
    relationship,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy
from zope.sqlalchemy import ZopeTransactionExtension


DBSession = scoped_session(
    sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


class Blog(Base):
    __tablename__ = 'blog'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    description = Column(UnicodeText)
    created = Column(DateTime, default=datetime.now)
    edited = Column(DateTime, default=datetime.now,
                    onupdate=datetime.now)


class BlogEntry(Base):
    __tablename__ = 'blog_entry'
    id = Column(Integer, primary_key=True)
    title = Column(Unicode(255))
    slug = Column(Unicode(255))
    date = Column(Date)
    contents = Column(UnicodeText)

    blog_id = Column(Integer, ForeignKey('blog.id'))
    blog = relationship('Blog', backref="entries")

    created = Column(DateTime, default=datetime.now)
    edited = Column(DateTime, default=datetime.now,
                    onupdate=datetime.now)

    tags = association_proxy('entry_tags', 'name',
                             creator=lambda name: EntryTag(name=name))

    def __str__(self):
        return "<BlogEntry title={self.title}>".format(self=self)

    def __repr__(self):
        return str(self)


class EntryImage(Base):
    __tablename__ = 'image'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    data = Column(BLOB)
    blog_entry_id = Column(Integer, ForeignKey('blog_entry.id'))
    entry = relationship('BlogEntry', backref='images')


class EntryTag(Base):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    blog_entry_id = Column(Integer, ForeignKey('blog_entry.id'))
    entry = relationship('BlogEntry', backref='entry_tags')
