.. -*- coding:utf-8 -*-

models
================


>>> from aodag.blog import models as m
>>> blog = m.Blog()


BlogEntry
-------------------

>>> entry = m.BlogEntry(blog=blog, title="ぶろぐ")
>>> entry in blog.entries
True
>>> blog.entries  # doctest:+ELLIPSIS
[<BlogEntry title=ぶろぐ>]
>>> entry.images
[]

Entry.tags
~~~~~~~~~~~~~~~~~~

>>> entry.tags
[]
>>> entry.tags.append("tag1")
>>> entry.tags
['tag1']
>>> entry.entry_tags[0].name
'tag1'
