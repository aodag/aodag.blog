# -*- coding:utf-8 -*-

import unittest
import webtest
from testfixtures import compare, Comparison as C


class Test(unittest.TestCase):

    def test(self):
        from aodag.blog import main
        app = main({})
        app = webtest.TestApp(app)

        params = [
            ('contents', '内容'),
            ('date', '2014-01-01'),
            ('tags', 'あ, い, う'),
            ('title', 'タイトル'),
            ('slug', 'this-is-slug'),
            ('add', ''),
        ]
        res = app.post('/new_entry', params=params)

        print(res.text)
        compare(res.location, 'http://localhost/entry/this-is-slug')
