import unittest
import doctest
from testfixtures import compare, Comparison as C


class TestBlog(unittest.TestCase):
    def _getTarget(self):
        from ..models import Blog
        return Blog

    def _makeOne(self, *args, **kwargs):
        return self._getTarget()(*args, **kwargs)

    def test_it(self):
        target = self._makeOne()
        compare(target.entries, [])


class TestBlogEntry(unittest.TestCase):
    def _getTarget(self):
        from ..models import BlogEntry
        return BlogEntry

    def _makeOne(self, *args, **kwargs):
        return self._getTarget()(*args, **kwargs)

    def test_it(self):
        target = self._makeOne()
        compare(target.blog, None)


def load_tests(loader, tests, pattern):
    tests.addTests(doctest.DocFileSuite('../models.txt'))
    return tests
