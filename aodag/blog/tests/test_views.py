# -*- coding:utf-8 -*-
from datetime import date
import unittest
from testfixtures import compare, Comparison as C


class TestBlogEntrySchema(unittest.TestCase):
    def _getTarget(self):
        from ..views import BlogEntrySchema
        return BlogEntrySchema

    def _makeOne(self, *args, **kwargs):
        return self._getTarget()(*args, **kwargs)

    def test_it(self):
        import colander
        target = self._makeOne()
        data = {
            "title": "タイトル",
            "date": "2014-01-01",
            "contents": "内容",
            "tags": ["あ", "い", "う"]
        }

        result = target.deserialize(data)

        compare(result, {
            'contents': '内容',
            'date': date(2014, 1, 1),
            'tags': ['あ', 'い', 'う'],
            'title': 'タイトル',
            'slug': colander.null,
        })
