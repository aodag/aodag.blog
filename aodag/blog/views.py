import colander as c
import deform.widget as w
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from pyramid_deform import FormView
from .models import (
    BlogEntry,
    Blog,
    DBSession,
)


class BlogEntrySchema(c.Schema):
    title = c.SchemaNode(c.String())
    slug = c.SchemaNode(c.String(), missing=c.null)
    date = c.SchemaNode(c.Date())
    contents = c.SchemaNode(c.String())
    tags = c.SchemaNode(c.List(),
                        widget=w.TextInputCSVWidget())


@view_config(route_name='new_entry', renderer="form.mako")
class BlogEntryForm(FormView):
    schema = BlogEntrySchema()
    buttons = ('add',)

    @property
    def blog(self):
        blog = Blog()
        DBSession.add(blog)
        return blog

    def add_success(self, values):
        entry = BlogEntry(**values)
        self.blog.entries.append(entry)
        location = self.request.route_url('entry', slug=entry.slug)
        return HTTPFound(location=location)
