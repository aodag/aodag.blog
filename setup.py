import os
from setuptools import setup, find_packages

__author__ = "Atsushi Odagiri"
__version__ = '0.0'

requires = [
    "setuptools>=0.7",
    "pyramid>=1.5dev",
    "pyramid_tm",
    "pyramid_mako",
    "sqlalchemy",
    "zope.sqlalchemy",
    "deform>=2.0dev",
    "pyramid_deform",
    "pyramid_layout",
    "pillow",
]

tests_require = [
    "webtest",
    "testfixtures",
]

here = os.path.dirname(__file__)


def _read(name):
    try:
        with open(os.path.join(here, name)) as f:
            return f.read()
    except:
        return ""

readme = _read('README.rst')

setup(name="aodag.blog",
      version=__version__,
      author=__author__,
      author_email="aodagx@gmail.com",
      license="MIT",
      long_description=readme,
      packages=find_packages(),
      namespace_packages=["aodag"],
      test_suite="aodag.blog",
      install_requires=requires,
      tests_require=tests_require,
      extras_require={
          "testing": tests_require,
          "dev": tests_require + ["waitress"],
      },
      )
